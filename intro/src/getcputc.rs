include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

fn main() {
    let mut c:i32 = 0;
    loop {
        unsafe {
            c = getc(__stdinp);
            if c == -1 {
                break;
            }

            if putc(c, __stdoutp) == EOF {
                println!("output error");
            }
        }
    }
}