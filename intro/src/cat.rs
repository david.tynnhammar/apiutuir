
// TODO: this doesn't really works, for any input it gets read error
include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

const BUFFSIZE:usize = 1024;

fn main() {
    
    let mut n;
    let buffer: &[u8] = &[0; BUFFSIZE];
    let buf = buffer.as_ptr() as *mut std::os::raw::c_void;

    loop {
        unsafe {
            n = read(STDIN_FILENO as i32, buf, BUFFSIZE);

            if n > 0 {
                if write(STDOUT_FILENO as i32, buf, n as usize) != n {
                    println!("write error");
                }
            } else {
                break;
            }
        }
    }

    if n < 0 {
        println!("read error {}", n);
    }
}
