use std::ffi::CString;
use std::ffi::CStr;
use std::env;

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

fn main() {
    let dirarg = env::args().nth(1).expect("missing argument");

    unsafe {
        let dir = opendir(CString::new(dirarg).unwrap().as_ptr());
        if dir.is_null() {
            println!("directory does not exist");
            return;
        }

        loop {
            let dirp = readdir(dir);
            if dirp.is_null() {
                break;
            } else {
                println!("{:?}", CStr::from_ptr((*dirp).d_name.as_ptr()));
            }
        }
    }
}
